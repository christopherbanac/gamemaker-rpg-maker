// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_drawFlag(){
	//facing north
	if (global.z >= 0 and global.z < 90)
	{
		global.northFlag=0;
		global.eastFlag=0;
		global.westFlag=1;
		global.southFlag=1;
	}
	//facing east
	if (global.z >= 90 and global.z < 180)
	{
		global.northFlag=1;
		global.eastFlag=0;
		global.westFlag=1;
		global.southFlag=0;
	}
	//facing south
	if (global.z >= 180 and global.z < 270)
	{
		global.northFlag=1;
		global.eastFlag=1;
		global.westFlag=0;
		global.southFlag=0;
	}
	//facing west
	if (global.z >= 270 and global.z < 360)
	{
		global.northFlag=0;
		global.eastFlag=1;
		global.westFlag=0;
		global.southFlag=1;
	}
}