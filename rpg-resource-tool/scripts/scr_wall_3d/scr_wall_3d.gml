// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_wall_3d(){
	
	x_axis = lengthdir_x(1,global.z-90);
	y_axis = lengthdir_y(1,global.z-90);
	tileSize = 64;
	westFlag = global.westFlag;
	southFlag = global.southFlag;
	northFlag= global.northFlag;
	eastFlag= global.eastFlag;
	
	//FLOOR
	draw_sprite_ext(sprite_index,5,x,y,image_xscale,image_yscale,direction,c_white,1);
	
	// LEFT/West
	draw_sprite_pos(sprite_index,1,x+tileSize*x_axis,y-tileSize*y_axis, (x+tileSize*x_axis),(y+tileSize)-tileSize*y_axis ,x,y+tileSize,x,y,westFlag);
	
	// FRONT/South
	draw_sprite_pos(sprite_index,2,(x+tileSize*x_axis),(y+tileSize)-tileSize*y_axis, (x+tileSize)+tileSize*x_axis,(y+tileSize)-tileSize*y_axis ,x+tileSize,y+tileSize,x,y+tileSize,southFlag);
	
	// RIGHT/East
	draw_sprite_pos(sprite_index,3, (x+tileSize)+tileSize*x_axis,(y+tileSize)-tileSize*y_axis ,(x+tileSize)+tileSize*x_axis,y-tileSize*y_axis, x+tileSize,y,x+tileSize,y+tileSize,eastFlag);
	
	// BACK/North
	draw_sprite_pos(sprite_index,4, (x+tileSize)+tileSize*x_axis,y-tileSize*y_axis ,x+tileSize*x_axis,y-tileSize*y_axis, x,y,x+tileSize,y,northFlag);
	
	// ROOF
	draw_sprite_ext(sprite_index,0,x+tileSize*x_axis,y-tileSize*y_axis,image_xscale,image_yscale,direction,c_white,1);
}