/// @function generator_load_level(level_number);
/// @description Generates a given level by creating instances of its objects
/// @param {real} Level number to load
/// @return {boolean} Truthy; whether the level was successfully generated
function generator_load_level(argument0) {

	// destroy all level-related instances
	layer_destroy_instances("layer_level");
	layer_destroy_instances("layer_floor");
	instance_destroy (oPlayer);

	var level_number = argument0;
	var baseFloor = oDunTiles;

	// check to ensure the level index is not out of bounds
	if (level_number >= array_length_1d(level_data)) return false;

	// temporary array for the specific level
	var level_array = level_data[level_number];

	// iterate over every array index, row by row, column by column
	for (var i = 0; i < array_height_2d(level_array); i++) { 
	    for (var j = 0; j < array_length_2d(level_array, i); j++) { 
		
			// read the string at the array indices
			var character = level_array[i, j];
		
			// lookup the object associated with the character
			var object = generator_character_lookup(character);
			
			//generate floor
			instance_create_layer(i * cell_width, j * cell_height, "layer_floor", baseFloor);
			// ensure the object is defined
	        if (object == undefined) continue;
		
			// create the instance at given coordinates
	        
	        instance_create_layer(i * cell_width, j * cell_height, "layer_level", object);
	    }
	}

	return true;


}
