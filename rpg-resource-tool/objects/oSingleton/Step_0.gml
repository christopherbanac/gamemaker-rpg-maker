/// @description Insert description here
// You can write your code in this editor



// create a level layer
layer_create(0, "layer_level");
layer_create(0, "layer_floor");
 
// holds level data
level_data = -1;

// the current level being displayed
current_level = 0;

// symbol used to denote the end of a level
level_separator = "END";

// objects will spawn at these (x, y) intervals
cell_width = 16;
cell_height = 16; 

// load levels.txt level data into memory
generator_import_levels();